from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.
@login_required
def receipt_list(request):
  receipt_list = Receipt.objects.filter(purchaser=request.user)
  context = {
    "receipt_list": receipt_list
  }
  return render(request, "receipts/list.html", context)

@login_required
def category_list(request):
  category_list = ExpenseCategory.objects.filter(owner=request.user)
  context = {
    "category_list": category_list
  }
  return render(request, "receipts/category.html", context)

@login_required
def account_list(request):
  account_list = Account.objects.filter(owner=request.user)
  context = {
    "account_list": account_list
  }
  return render(request, "receipts/account.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            form.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "post_form": form,
    }
    return render(request, "receipts/create.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            form.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "post_form": form,
    }
    return render(request, "receipts/create_category.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            form.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "post_form": form,
    }
    return render(request, "receipts/create_account.html", context)
